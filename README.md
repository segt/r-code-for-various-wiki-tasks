# R-Code-For-Various-Wiki-Tasks

## Description
This is a place for me, Silvia Gutiérrez (User:SEgt-WMF), to share some code I do for different "wiki" taks

## Usage
Most code is commented and you're free to re-use it if it's useful for you CC-BY-4.0 would be appreciated it :)

## Support / Contributing
I'm not really mantaining this code, but feel free to drop me a line if you have a question or if you would like to contribute! I'll do my best to answer

## Authors and acknowledgment
I'll add appreciation notes here for collaborators :heart:

## License
CC-BY-4.0
